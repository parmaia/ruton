package com.parmaia.ruton.actividades;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.parmaia.ruton.R;
import com.parmaia.ruton.enums.Modo;

public class Inicio  extends SherlockActivity{

  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    ActionBar ab = getSupportActionBar();
    ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE|ActionBar.DISPLAY_SHOW_HOME); //|ActionBar.DISPLAY_HOME_AS_UP
    ab.setTitle(getString(R.string.app_name));
    
    setContentView(R.layout.inicio);
    
    findViewById(R.id.crear).setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        crearRuta();
      }
    });
    findViewById(R.id.seguir).setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        seguirRuta();
      }
    });
    super.onCreate(savedInstanceState);
  }

  protected void crearRuta() {
    Intent i = new Intent(this, SelRuta.class);
    i.putExtra(Modo.tag(), Modo.CREAR.toString());
    startActivity(i);
  }

  protected void seguirRuta() {
    Intent i = new Intent(this, SelRuta.class);
    i.putExtra(Modo.tag(), Modo.SEGUIR.toString());
    startActivity(i);
  }
}
