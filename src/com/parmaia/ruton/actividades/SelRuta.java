package com.parmaia.ruton.actividades;

import java.util.List;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.parmaia.ruton.R;
import com.parmaia.ruton.dao.CoordRuta;
import com.parmaia.ruton.dao.IndRuta;
import com.parmaia.ruton.dao.Ruta;
import com.parmaia.ruton.dialogos.DialogoInput;
import com.parmaia.ruton.dialogos.DialogoSiNo;
import com.parmaia.ruton.dialogos.DialogoInput.DialogoInputListener;
import com.parmaia.ruton.dialogos.DialogoSiNo.DialogoSiNoListener;
import com.parmaia.ruton.enums.Modo;
import com.parmaia.ruton.util.RutonApp;

public class SelRuta extends SherlockListActivity {
    RutasAdapter adapter;
    protected Object actionMode;
    private int itemSeleccionado;
    
    Modo modo;
    private List<Ruta> rutas;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new RutasAdapter();
        setListAdapter(adapter);
        //TODO: Leer http://www.vogella.com/articles/AndroidActionBar/article.html
        
        ActionBar ab = getSupportActionBar();
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE|ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_HOME_AS_UP); 
        modo = Modo.fromString(getIntent().getExtras().getString(Modo.tag()));
        
        switch(modo){
          case CREAR:
            ab.setTitle(getString(R.string.crear));
            break;
          case NINGUNO:
            ab.setTitle(getString(R.string.sel_ruta));
            break;
          case SEGUIR:
            ab.setTitle(getString(R.string.seguir));
            break;
        }
        if (modo==Modo.CREAR){
          getListView().setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
              if (actionMode != null) {
                return false;
              }
              itemSeleccionado = position;
              // start the CAB using the ActionMode.Callback defined above
              actionMode = getSherlock().startActionMode(actionModeCallback);
              view.setSelected(true);
              return true;
            }
          });
        }
    }
    
    @Override
    protected void onResume() {
      cargarRutas();
      super.onResume();
    }
    
    private void cargarRutas() {
      rutas = RutonApp.getInstance().getRutonDao().getDaoSession().getRutaDao().loadAll();
      adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
      if (modo==Modo.CREAR){
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.inicio, menu);
        return true;
      }
      return false;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()){
        case android.R.id.home:
          finish();
          break;
        case R.id.nuevo:
          Intent i = new Intent(this, Nueva.class);
          i.putExtra(Modo.tag(), Modo.CREAR.toString());
          startActivity(i);
          break;
        }
        return true;
    }
    
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
      cargarRutaCompleta(position);
      Intent i;
      switch(modo){
        case SEGUIR:
          i = new Intent(this, SeguirRuta.class);
          i.putExtra("ruta", rutas.get(position));
          startActivity(i);
          break;
        case CREAR:
          i = new Intent(this, Indicaciones.class);
          i.putExtra("ruta", rutas.get(position));
          startActivity(i);
          break;
        case NINGUNO:
          break;
      }
      super.onListItemClick(l, v, position, id);
    }    
    
    private void cargarRutaCompleta(int position){
      //Los reset son para forzar la recarga
      rutas.get(position).resetCoordRutaList();
      rutas.get(position).resetIndRutaList();
      rutas.get(position).getCoordRutaList();
      rutas.get(position).getIndRutaList();
    }
    
    private class RutasAdapter extends BaseAdapter{

      @Override
      public int getCount() {
        return rutas==null?0:rutas.size();
      }

      @Override
      public Object getItem(int pos) {
        return pos;
      }

      @Override
      public long getItemId(int pos) {
        return rutas==null?-1:rutas.get(pos).getId();
      }

      private class Holder{
        TextView texto;
      }
      
      @Override
      public View getView(int pos, View contentView, ViewGroup group) {
        Holder holder;
        if (contentView==null){
          LayoutInflater inflater = SelRuta.this.getLayoutInflater();
          contentView = inflater.inflate(R.layout.ruta_item, null);
          holder = new Holder();
          holder.texto = (TextView)contentView.findViewById(R.id.texto);
          contentView.setTag(holder);
        }else{
          holder = (Holder) contentView.getTag();
        }
        
        holder.texto.setText(rutas.get(pos).getNombre());
        
        return contentView;
      }
      
    }
    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
      // called when the action mode is created; startActionMode() was called
      public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        // Inflate a menu resource providing context menu items
        MenuInflater inflater = mode.getMenuInflater();
        // assumes that you have "contexual.xml" menu resources
        inflater.inflate(R.menu.eliminar_renombrar, menu);
        return true;
      }

      // the following method is called each time 
      // the action mode is shown. Always called after
      // onCreateActionMode, but
      // may be called multiple times if the mode is invalidated.
      public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false; // Return false if nothing is done
      }

      // called when the user selects a contextual menu item
      public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
          case R.id.renombrar:
            iniciaDialogoEditar(mode);
            return true;
          case R.id.eliminar:
            iniciaDialogoEliminar(mode);
//            indicaciones.get(itemSeleccionado).getMarca().remove();
//            indicaciones.remove(itemSeleccionado);
//            adapter.notifyDataSetChanged();
            return true;
        default:
          return false;
        }
      }

      // called when the user exits the action mode
      public void onDestroyActionMode(ActionMode mode) {
        actionMode = null;
        itemSeleccionado = -1;
      }
    };
    protected void iniciaDialogoEditar(final ActionMode mode) {
      (new DialogoInput(SelRuta.this)).setTitulo(getResources().getString(R.string.nombre_ruta)).
                setTexto(rutas.get(itemSeleccionado).getNombre()).
                setDialogoInputListener(new DialogoInputListener() {
        @Override
        public void onCancelar() {
          if (mode!=null) mode.finish();
        }
        @Override
        public void onAceptar(String texto) {
          rutas.get(itemSeleccionado).setNombre(texto);
          rutas.get(itemSeleccionado).update();
          adapter.notifyDataSetChanged();
          if (mode!=null) mode.finish();
        }
      }).show();
    }      
    protected void iniciaDialogoEliminar(final ActionMode mode) {
      (new DialogoSiNo(SelRuta.this)).setTitulo(getResources().getString(R.string.seguro_eliminar_ruta)).
                setDialogoSiNoListener(new DialogoSiNoListener() {
        @Override
        public void onCancelar() {
          if (mode!=null) mode.finish();
        }
        @Override
        public void OnAceptar() {
          RutonApp.getInstance().getRutonDao().getDaoSession().getCoordRutaDao().deleteInTx(rutas.get(itemSeleccionado).getCoordRutaList());
          RutonApp.getInstance().getRutonDao().getDaoSession().getIndRutaDao().deleteInTx(rutas.get(itemSeleccionado).getIndRutaList());
          rutas.get(itemSeleccionado).delete();
          rutas.remove(itemSeleccionado);
          adapter.notifyDataSetChanged();
          if (mode!=null) mode.finish();
        }
      }).show();
    }      
}
