package com.parmaia.ruton.actividades;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationClient.OnAddGeofencesResultListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.parmaia.ruton.R;
import com.parmaia.ruton.adapters.MiInfoWindowAdapter;
import com.parmaia.ruton.dao.IndRuta;
import com.parmaia.ruton.dao.Ruta;
import com.parmaia.ruton.servicios.ServicioRuton;
import com.parmaia.ruton.util.Log;
import com.parmaia.ruton.util.Mensaje;
import com.parmaia.ruton.util.RutonApp;
import com.parmaia.ruton.util.SysUtils;

public class SeguirRuta extends SherlockFragmentActivity {

  private static final LocationRequest REQUEST = LocationRequest.create()
      .setInterval(5000)         // 5 seconds
      .setFastestInterval(1000)    // 16ms = 60fps
      .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
  
  private Ruta ruta;
  private GoogleMap mapa;
  private boolean mostrarDialogoGPS=true;
  private LocationClient mLocationClient;
  private ArrayList<LatLng> trayecto = new ArrayList<LatLng>();

  protected float zoom;

  private Polyline trackActual;
    
  /* Para los GeoFences */
  
  // Stores the PendingIntent used to request geofence monitoring
  private PendingIntent mGeofenceRequestIntent;
  // Defines the allowable request types.
  public enum REQUEST_TYPE {
    ADD
  };
  
  private REQUEST_TYPE mRequestType;
  // Flag that indicates if a request is underway.
  private boolean mInProgress;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.l();
    
    ActionBar ab = getSupportActionBar();
    ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE
        | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
    ab.setTitle(getString(R.string.en_ruta));
    Log.l();
    setContentView(R.layout.seguir_ruta);
    Log.l();
    if (getIntent().getExtras().containsKey("ruta")) {
      Log.l();
      ruta = (Ruta) getIntent().getExtras().get("ruta");
      Log.l();
    } else {
      Mensaje.corto(this, R.string.no_hay_ruta);
      finish();
    }
    mInProgress=false;
    Log.l();
    SupportMapFragment mapFragment =
        (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

    if (savedInstanceState == null) {
      // First incarnation of this activity.
      Log.l();
      RutonApp.getInstance().setIndicacionActual(0);//Estamos comenzando una ruta nueva
      mapFragment.setRetainInstance(true);
    } else {
      // Reincarnated activity. The obtained map is the same map instance in the previous
      // activity life cycle. There is no need to reinitialize it.
      Log.l();
      if (savedInstanceState.containsKey("ruta") && savedInstanceState.containsKey("trayecto")){
        ruta = (Ruta) savedInstanceState.get("ruta");
        trayecto = savedInstanceState.getParcelableArrayList("trayecto");
      }
      Log.l();
      mapa = mapFragment.getMap();
    }
    Log.l();
    getMapa(); //Por si hace falta
    Log.l();
  }

  @Override
  protected void onResume() {
    super.onResume();
    Log.l();
    getMapa();
    setMiUbicacion();
    setupLocationClientIfNeeded();
    mLocationClient.connect();
  }

  @Override
  public void onPause() {
      super.onPause();
      if (mLocationClient != null) {
          mLocationClient.disconnect();
      }
  }
  
  @Override
  protected void onSaveInstanceState(Bundle outState) {
    Log.l();
    outState.putSerializable("ruta", ruta);
    outState.putParcelableArrayList("trayecto", trayecto);
    super.onSaveInstanceState(outState);
  }
  
  private void getMapa() {
    Log.l();
    if (mapa == null) {
      mapa = ((SupportMapFragment) getSupportFragmentManager()
          .findFragmentById(R.id.map)).getMap();
      if (mapa != null) {
        // if (ruta.getId() != null)
        // atacharSesionARuta();
        setupMapa();
        montarIndicaciones();
      }
    }
  }


  private void setupLocationClientIfNeeded() {
    Log.l();
    if (mLocationClient == null) {
          mLocationClient = new LocationClient(
                  getApplicationContext(),
                  new ConnectionCallbacks() {                    
                    @Override
                    public void onDisconnected() { 
                      Log.l("LocationClient Desconectado");
                      mLocationClient=null;
                    }
                    
                    @Override
                    public void onConnected(Bundle connectionHint) {
                      zoom=17;
                      mLocationClient.requestLocationUpdates(
                          REQUEST,
                          new LocationListener() {                            
                            @Override
                            public void onLocationChanged(Location location) {
                              //TODO Agregar un flag para hacer o no esto siempre.
                              mapa.animateCamera(CameraUpdateFactory.newCameraPosition(                                  
                                  new CameraPosition(new LatLng(location.getLatitude(),location.getLongitude()), zoom, 0, 0)));
                              trayecto.add(new LatLng(location.getLatitude(),location.getLongitude()));
                              trackActual.setPoints(trayecto);
                            }
                          });  // LocationListener
                      //TODO Llamar al servicio.
//                      mLocationClient.requestLocationUpdates(REQUEST, callbackIntent);
                      setupIndicaciones();
                    }
                  },  // ConnectionCallbacks
                  new OnConnectionFailedListener(){
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                      Log.l("Connection Failed, error code: ",result.getErrorCode());
                      mLocationClient=null;
                    }
                  }); // OnConnectionFailedListener
      }
  }
  
  private void setMiUbicacion(){
    SysUtils.isGPSOn(this, mostrarDialogoGPS);
    Log.l();
    mapa.setMyLocationEnabled(true);    
    Log.l();
//    mostrarDialogoGPS=false;
  }
  
  private void setupMapa() {
    Log.l();
    mapa.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    if (ruta.getSize()>0){
      //mapa.addPolyline(ruta.getPolylineOptions());
      List<PolylineOptions> trozos = ruta.getPolylineOptions(4);
      for (PolylineOptions p : trozos){
        mapa.addPolyline(p);
      }
      
      mapa.getUiSettings().setCompassEnabled(true);
      mapa.getUiSettings().setMyLocationButtonEnabled(false);
      
      trackActual=mapa.addPolyline((new PolylineOptions().width(2).color(Color.GREEN)));

      agregarMarca(BitmapDescriptorFactory.fromResource(R.drawable.pin_verde), 
          ruta.getInicio(), 
          getResources().getString(R.string.inicio), "", false);
    
      agregarMarca(BitmapDescriptorFactory.fromResource(R.drawable.pin_rojo), 
          ruta.getFin(), 
          getResources().getString(R.string.fin), "", false);
      mapa.moveCamera(CameraUpdateFactory.scrollBy(1, 1));//para forzar el OnCameraChange
      mapa.setOnCameraChangeListener(new OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition arg0) {
          mapa.animateCamera(CameraUpdateFactory.newLatLngBounds( ruta.getLatLngBounds() , 20));
          mapa.setOnCameraChangeListener(new OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {
              //TODO iniciar un timer o un handler con un timeout para cuando mueva el mapa, pasado un tiempo se centre en mi ubicacion otra vez.
              zoom = position.zoom;
            }
          });
        }
      });
    }
    mapa.setInfoWindowAdapter(new MiInfoWindowAdapter(this));
  }

  private void montarIndicaciones() {
    if (ruta.numIndicaciones() > 0) {
      for (IndRuta indRuta : ruta.getIndRutaList()) {
        LatLng position = new LatLng(indRuta.getLat(), indRuta.getLon());
        agregarMarca(BitmapDescriptorFactory
            .fromResource(R.drawable.pin_azul), position,
            indRuta.getTexto(), "" , false);
        Circle c = mapa.addCircle(new CircleOptions().center(position).radius(RutonApp.getInstance().getConfig().radioIndicacion).
            fillColor(0x0021ff31).strokeColor(0x0000FF62).strokeWidth(8));
      }
    }
  }

  private Marker agregarMarca(BitmapDescriptor bitmapDescriptor, LatLng latLng,
      String titulo, String snippet, boolean draggable) {
    
    return mapa.addMarker((new MarkerOptions()).anchor(0.775f, 0.9f)
        .icon(bitmapDescriptor).draggable(draggable).position(latLng)
        .title(titulo).snippet(snippet));
  }
  
  /**
   * Se llama una vez que el LocationClient se ha conectado
   */
  private void setupIndicaciones(){
    Log.l();
 // Get the PendingIntent for the request
    mGeofenceRequestIntent = getTransitionPendingIntent();
    // Send a request to add the current geofences
    Log.l("agregando geofence");
    List<Geofence> proxIndicacion = Collections.singletonList(
      ruta.getIndRutaList().get(RutonApp.getInstance().getIndicacionActual()).getGeofence()
      );
    mLocationClient.addGeofences(proxIndicacion, mGeofenceRequestIntent, new OnAddGeofencesResultListener() {
      @Override
      public void onAddGeofencesResult(int statusCode, String[] geofenceRequestIds) {
        switch (statusCode){
          case LocationStatusCodes.SUCCESS: Log.l("statusCode: SUCCESS"); break;
          case LocationStatusCodes.ERROR: Log.l("statusCode: ERROR"); break;
          case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE: Log.l("statusCode: GEOFENCE_NOT_AVAILABLE"); break;
          case LocationStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES: Log.l("statusCode: GEOFENCE_TOO_MANY_GEOFENCES"); break;
          case LocationStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS: Log.l("statusCode: GEOFENCE_TOO_MANY_PENDING_INTENTS"); break;
        }
        if (geofenceRequestIds.length>0){
          String ids="";
          for (String id : geofenceRequestIds){
            ids += id+" ";
          }
          Log.l("Agregados los geofences (",geofenceRequestIds.length,"): ", ids);
        }
      }      
    });
  }
  
  private PendingIntent getTransitionPendingIntent() {
    // Create an explicit Intent
    Intent intent = new Intent(this, ServicioRuton.class);
    intent.putExtra(ServicioRuton.KEY_EVENTO, ServicioRuton.EVENTO_INDICACION);
    /*
     * Return the PendingIntent
     */
    return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }
}
