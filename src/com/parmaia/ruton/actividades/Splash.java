package com.parmaia.ruton.actividades;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.parmaia.ruton.R;
import com.parmaia.ruton.R.layout;
import com.parmaia.ruton.util.Log;
import com.parmaia.ruton.util.Mensaje;
import com.parmaia.ruton.util.RutonApp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

/**
 * Created by parmaia on 10/07/13.
 */
public class Splash extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        startInit();
    }

    private void startInit(){
        if (!RutonApp.getInstance().isIniciado()){
            new AsyncTask<Void, Void, Integer>(){
                @Override
                protected Integer doInBackground(Void... voids) {
                  int resultCode =ConnectionResult.SERVICE_DISABLED;  
                  try {
                        Thread.sleep(500);
                        //int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(Splash.this);
                        resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(Splash.this);
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return resultCode;
                }

                @Override
                protected void onPostExecute(Integer resultCode) {
                    super.onPostExecute(resultCode);
                    if (resultCode != ConnectionResult.SUCCESS) {
                      if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                        GooglePlayServicesUtil.getErrorDialog(resultCode, Splash.this, 9000).show();                        
                      } else {
                        Log.i("GCM", "This device is not supported.");
                        Mensaje.largo(Splash.this, R.string.google_play_no_disponible);
                        finish();
                      }
                    }else{
                        RutonApp.getInstance().setIniciado(true);
                        iniciarApp();
                    }
                }
            }.execute();
        }else{
            iniciarApp();
        }
    }

    private void iniciarApp(){
        startActivity(new Intent(Splash.this, Inicio.class));
        finish();
    }
    
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    // Decide what to do based on the original request code
    switch (requestCode) {
      case 9000:
        /*
         * If the result code is Activity.RESULT_OK, try to connect again
         */
        switch (resultCode) {
          case Activity.RESULT_OK:
            startInit();
            break;
          default:
            Log.i("GCM", "This device is not supported. ",resultCode);
            Mensaje.largo(this, "Servicios de Google Play no disponibles");
            finish();
            break;
        }
    }
    super.onActivityResult(requestCode, resultCode, data);
  }
}
