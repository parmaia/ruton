package com.parmaia.ruton.actividades;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.parmaia.ruton.R;
import com.parmaia.ruton.adapters.MiInfoWindowAdapter;
import com.parmaia.ruton.dao.CoordRuta;
import com.parmaia.ruton.dao.CoordRutaDao;
import com.parmaia.ruton.dao.IndRuta;
import com.parmaia.ruton.dao.IndRutaDao;
import com.parmaia.ruton.dao.Ruta;
import com.parmaia.ruton.dao.RutaDao;
import com.parmaia.ruton.dialogos.DialogoIndicaciones;
import com.parmaia.ruton.dialogos.DialogoIndicaciones.DialogoIndicacionesListener;
import com.parmaia.ruton.entidades.Indicacion;
import com.parmaia.ruton.util.Log;
import com.parmaia.ruton.util.Mensaje;
import com.parmaia.ruton.util.RutonApp;

public class Indicaciones extends SherlockFragmentActivity {
  private GoogleMap mapa;
  private Ruta ruta;
  private boolean agregandoIndicacion=false;
  private Marker ultimaMarca;
  private List<Indicacion> indicaciones;
  private ListView lstIndicaciones;
  private IndicacionesAdapter adapter;
  protected Object actionMode;
  public int itemSeleccionado = -1;
    
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    ActionBar ab = getSupportActionBar();
    ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
    ab.setTitle(getString(R.string.indicaciones));
    
    setContentView(R.layout.indicaciones);

    indicaciones = new ArrayList<Indicacion>();
    lstIndicaciones = (ListView) findViewById(R.id.indicaciones);
    adapter = new IndicacionesAdapter();
    lstIndicaciones.setAdapter(adapter);
    lstIndicaciones.setOnItemLongClickListener(new OnItemLongClickListener() {
      @Override
      public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        if (actionMode != null) {
          return false;
        }
        itemSeleccionado = position;
        mapa.animateCamera(CameraUpdateFactory.newLatLng(indicaciones.get(position).getMarca().getPosition()));
        indicaciones.get(position).getMarca().showInfoWindow();
        // start the CAB using the ActionMode.Callback defined above
        actionMode = getSherlock().startActionMode(actionModeCallback);
        view.setSelected(true);
        return true;
      }
    });
    
    lstIndicaciones.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mapa.animateCamera(CameraUpdateFactory.newLatLng(indicaciones.get(position).getMarca().getPosition()));
        indicaciones.get(position).getMarca().showInfoWindow();
      }
    });
    
    if (getIntent().getExtras().containsKey("ruta")){
      ruta = (Ruta) getIntent().getExtras().get("ruta");
    }else{
      Mensaje.corto(this, R.string.no_hay_ruta);
      finish();
    }

    getMapa();
  }
  
  private void montarIndicaciones() {
    if(ruta.numIndicaciones()>0){
      for (IndRuta indRuta : ruta.getIndRutaList()){
        Indicacion ind = new Indicacion();
        ind.setTexto(indRuta.getTexto());
        ind.setMarca(
            agregarMarca(BitmapDescriptorFactory.fromResource(R.drawable.pin_azul), 
                         new LatLng(indRuta.getLat(),indRuta.getLon()), 
                         getResources().getString(R.string.nueva_indicacion), ind.getTexto(), true )
        );
        indicaciones.add(ind);
      }
      adapter.notifyDataSetChanged();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    getMapa();
  }

  private void setupMapa() {
    mapa.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//    mapa.setMyLocationEnabled(true);
    if (ruta.getSize()>0){
      //mapa.addPolyline(ruta.getPolylineOptions());
      List<PolylineOptions> trozos = ruta.getPolylineOptions(4);
      for (PolylineOptions p : trozos){
        mapa.addPolyline(p);
      }

      agregarMarca(BitmapDescriptorFactory.fromResource(R.drawable.pin_verde), 
          ruta.getInicio(), 
          getResources().getString(R.string.inicio), "", false);
    
      agregarMarca(BitmapDescriptorFactory.fromResource(R.drawable.pin_rojo), 
          ruta.getFin(), 
          getResources().getString(R.string.fin), "", false);
      mapa.moveCamera(CameraUpdateFactory.scrollBy(1, 1));//para forzar el OnCameraChange
      mapa.setOnCameraChangeListener(new OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition arg0) {
          mapa.animateCamera(CameraUpdateFactory.newLatLngBounds( ruta.getLatLngBounds() , 20));
          mapa.setOnCameraChangeListener(null);
        }
      });
    }

    mapa.setOnMapLongClickListener(new OnMapLongClickListener() {      
      @Override
      public void onMapLongClick(LatLng point) {
        agregarNuevaIndicacion(point);        
      }
    });
    mapa.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
      @Override
      public void onInfoWindowClick(Marker marker) {
        if (!agregandoIndicacion){
          for (int i=0;i<indicaciones.size();i++){
            if (indicaciones.get(i).getMarca().getId().equals(marker.getId())){
              itemSeleccionado=i;
              iniciaDialogoEditar(null);              
            }
          }
        }
      }
    });
    mapa.setInfoWindowAdapter(new MiInfoWindowAdapter(this));
  }
  
  protected void agregarNuevaIndicacion(LatLng point) {
    if (!agregandoIndicacion){
      agregandoIndicacion=true;
      supportInvalidateOptionsMenu();
      ultimaMarca = agregarMarca(BitmapDescriptorFactory.fromResource(R.drawable.pin_azul), 
          point, 
          getResources().getString(R.string.nueva_indicacion), "", true);
    }
  }

  private void getMapa() {
    if (mapa == null){
      mapa = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
      if (mapa!=null){
        if (ruta.getId() != null)
          atacharSesionARuta();
        setupMapa();
        montarIndicaciones();
      }
    }
  }

  private void atacharSesionARuta() {
    //Para "atachar" la ruta a la sesion de la bd
    ruta = RutonApp.getInstance().getRutonDao().getDaoSession().getRutaDao().load(ruta.getId());
    //Los datos estan cacheados en la sesion y deberian estar actualizados.
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
      MenuInflater inflater = getSupportMenuInflater();
      if (agregandoIndicacion)
        inflater.inflate(R.menu.aceptar_cancelar, menu);
      else
        inflater.inflate(R.menu.guardar, menu);
      return true;
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case android.R.id.home:
        finish();
        break;
      case R.id.aceptar:
        (new DialogoIndicaciones(Indicaciones.this)).ocultarSubirBajar().setDialogoIndicacionesListener(new DialogoIndicacionesListener() {
          @Override
          public void onCancelar() {
            cancelarUltimaMarca();
          }
          @Override
          public void onAceptar(String texto) {
            indicaciones.add(new Indicacion(ultimaMarca,texto));
            ultimaMarca.setSnippet(texto);
            ultimaMarca.setTitle(getResources().getString(R.string.indicacion));
            if (ultimaMarca.isInfoWindowShown()){
              ultimaMarca.hideInfoWindow();
              ultimaMarca.showInfoWindow();
            }
            agregandoIndicacion = false;
            supportInvalidateOptionsMenu();
            adapter.notifyDataSetChanged();
          }
          @Override
          public void onBajar() {}
          @Override
          public void onSubir() {}
        }).show();
        break;
      case R.id.cancelar:
        cancelarUltimaMarca();
        break;
      case R.id.guardar:
        guardarRuta();
        break;
    }
    return true;
  }

  private void cancelarUltimaMarca(){
    ultimaMarca.remove();
    agregandoIndicacion=false;
    supportInvalidateOptionsMenu();
  }
  
  private class IndicacionesAdapter extends BaseAdapter{
    
    @Override
    public int getCount() {
      return indicaciones.size();
    }

    @Override
    public Object getItem(int pos) {
      return indicaciones.get(pos);
    }

    @Override
    public long getItemId(int pos) {
      return pos;
    }

    private class Holder{
      TextView texto;
    }
    
    @Override
    public View getView(int pos, View contentView, ViewGroup group) {
      Holder holder;
      if (contentView==null){
        LayoutInflater inflater = Indicaciones.this.getLayoutInflater();
        contentView = inflater.inflate(R.layout.indicacion_item, null);
        holder = new Holder();
        holder.texto = (TextView)contentView.findViewById(R.id.texto);
        contentView.setTag(holder);
      }else{
        holder = (Holder) contentView.getTag();
      }
      Log.l("Get item", pos);
      
      holder.texto.setText(indicaciones.get(pos).getTexto());
      
      return contentView;
    }
  }
  
  private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
    // called when the action mode is created; startActionMode() was called
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
      // Inflate a menu resource providing context menu items
      MenuInflater inflater = mode.getMenuInflater();
      // assumes that you have "contexual.xml" menu resources
      inflater.inflate(R.menu.indicacion_seleccionada, menu);
      return true;
    }

    // the following method is called each time 
    // the action mode is shown. Always called after
    // onCreateActionMode, but
    // may be called multiple times if the mode is invalidated.
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
      return false; // Return false if nothing is done
    }

    // called when the user selects a contextual menu item
    public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
      switch (item.getItemId()) {
        case R.id.editar:
          iniciaDialogoEditar(mode);
          return true;
        case R.id.eliminar:
          indicaciones.get(itemSeleccionado).getMarca().remove();
          indicaciones.remove(itemSeleccionado);
          adapter.notifyDataSetChanged();
          mode.finish();
          return true;
      default:
        return false;
      }
    }

    // called when the user exits the action mode
    public void onDestroyActionMode(ActionMode mode) {
      actionMode = null;
      itemSeleccionado = -1;
    }
  };

  protected void iniciaDialogoEditar(final ActionMode mode) {
    (new DialogoIndicaciones(Indicaciones.this)).
             setTitulo(getResources().getString(R.string.texto_indicacion)).
             setTexto(indicaciones.get(itemSeleccionado).getTexto()).
             setDialogoIndicacionesListener(new DialogoIndicacionesListener() {
      @Override
      public void onCancelar() {
        if (mode!=null) mode.finish();
      }
      @Override
      public void onAceptar(String texto) {
        indicaciones.get(itemSeleccionado).setTexto(texto);
        indicaciones.get(itemSeleccionado).getMarca().setSnippet(texto);
        if (indicaciones.get(itemSeleccionado).getMarca().isInfoWindowShown()){
          indicaciones.get(itemSeleccionado).getMarca().hideInfoWindow();
          indicaciones.get(itemSeleccionado).getMarca().showInfoWindow();
        }
        adapter.notifyDataSetChanged();
        if (mode!=null) mode.finish();
      }
      @Override
      public void onBajar() {
        if (itemSeleccionado<indicaciones.size()-1){
          indicaciones.add(itemSeleccionado+2, indicaciones.get(itemSeleccionado));
          indicaciones.remove(itemSeleccionado);
          itemSeleccionado++;
          adapter.notifyDataSetChanged();
        }
      }
      @Override
      public void onSubir() {
        if (itemSeleccionado>0){
          indicaciones.add(itemSeleccionado-1, indicaciones.get(itemSeleccionado));
          indicaciones.remove(itemSeleccionado+1);
          itemSeleccionado--;
          adapter.notifyDataSetChanged();
        }
      }
    }).show();
  }  
  
  private Marker agregarMarca(BitmapDescriptor bitmapDescriptor, LatLng latLng, String titulo, String snippet, boolean draggable){
    return mapa.addMarker((new MarkerOptions()).
      anchor(0.775f, 0.9f).
      icon(bitmapDescriptor).
      draggable(draggable).
      position(latLng).
      title(titulo).snippet(snippet)
      );
  }
    
  private void guardarRuta(){
    IndRutaDao idao = RutonApp.getInstance().getRutonDao().getDaoSession().getIndRutaDao();
    RutaDao rdao = RutonApp.getInstance().getRutonDao().getDaoSession().getRutaDao();
    if (ruta.getId()==null){
      rdao.insert(ruta);
      for( CoordRuta c : ruta.getCoordRutaList()){
        c.setRuta(ruta);
      }
      CoordRutaDao cdao = RutonApp.getInstance().getRutonDao().getDaoSession().getCoordRutaDao();
      cdao.insertInTx(ruta.getCoordRutaList());
    }else{
      ruta.update();
      idao.deleteInTx(ruta.getIndRutaList());      
    }
    List<IndRuta> indRuta = new ArrayList<IndRuta>();
    for (Indicacion ind : indicaciones){
      IndRuta ir = new IndRuta();
      ir.setLat(ind.getMarca().getPosition().latitude);
      ir.setLon(ind.getMarca().getPosition().longitude);
      ir.setTexto(ind.getTexto());
      ir.setRuta(ruta);
      indRuta.add(ir);
    }
    
    idao.insertInTx(indRuta);
    
    Mensaje.corto(this, R.string.ruta_guardada);
    
    Intent intent = new Intent(this, Inicio.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);    
    
  }
}
