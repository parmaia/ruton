package com.parmaia.ruton.actividades;

import java.io.File;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.parmaia.ruton.R;
import com.parmaia.ruton.dao.Ruta;
import com.parmaia.ruton.dialogos.DialogoSeleccionArchivo;
import com.parmaia.ruton.dialogos.FileDialog;
import com.parmaia.ruton.parsers.Parser;
import com.parmaia.ruton.parsers.Parser.ParseCallback;
import com.parmaia.ruton.util.Mensaje;
import com.parmaia.ruton.util.RutonApp;

public class Nueva extends SherlockActivity {

  private Button btnContinuar;
  private EditText txtNombre;
  protected boolean cargandoRuta=false;
  private Button btnImportar;
  protected Ruta ruta;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    ActionBar ab = getSupportActionBar();
    ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE
        | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
    ab.setTitle(getString(R.string.nuevo));

    setContentView(R.layout.nueva);
    btnImportar = (Button) findViewById(R.id.importar);
    btnImportar.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (!cargandoRuta){
          cargandoRuta=true;
          btnContinuar.setVisibility(View.INVISIBLE);
          txtNombre.setText("");
          
//          File f = new File("/storage/sdcard0/Rutas/ruta.gpx");
//          analizarRuta(f);
          seleccionarArchivo();
        }
      }
    });
    btnContinuar = (Button) findViewById(R.id.continuar);
    //TODO: http://docs.xamarin.com/recipes/android/resources/general/style_a_button/
    btnContinuar.setOnClickListener(new OnClickListener() {      
      @Override
      public void onClick(View v) {
        Intent i = new Intent(Nueva.this, Indicaciones.class);
        ruta.setNombre(txtNombre.getText().toString());
        i.putExtra("ruta", ruta);
        startActivity(i);
        btnContinuar.setEnabled(false);
      }
    });

    txtNombre = (EditText) findViewById(R.id.nombre);

  }

  @Override
  protected void onResume() {
    btnContinuar.setEnabled(true);
    super.onResume();
  }
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (cargandoRuta) return true;
    if (item.getItemId() == android.R.id.home) {
      // Intent intent = new Intent(this, Inicio.class);
      // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
      // startActivity(intent);
      finish();
      return true;
    }
    return true;
  }

  private void seleccionarArchivo() {
//    FileDialog fileDialog = new FileDialog(this, ".gpx");
//    fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
//      public void fileSelected(File file) {
//        Log.d(getClass().getName(), "selected file " + file.toString());
//        analizarRuta(file);
//      }
//    });
//    fileDialog.showDialog();
    DialogoSeleccionArchivo fileDialog = new DialogoSeleccionArchivo(this, ".gpx");
    fileDialog.addFileListener(new DialogoSeleccionArchivo.FileSelectedListener() {
      public void fileSelected(File file) {
        Log.d(getClass().getName(), "selected file " + file.toString());
        analizarRuta(file);
      }

      @Override
      public void cancelDialog() {
        cargandoRuta=false;
      }
    });
    fileDialog.showDialog();
    
  }

  protected void analizarRuta(File file) {
    //TODO: http://developer.android.com/training/basics/network-ops/xml.html
    RutonApp.getInstance().mostrarCargando(this, getResources().getString(R.string.cargando));
    Parser.parseTrack(file, new ParseCallback() {
      @Override
      public void onParseFail() {
        Mensaje.corto(Nueva.this, R.string.no_se_pudo_cargar_la_ruta);
        cargaFinalizada();
      }

      @Override
      public void onParseEnd(Parser parser) {
        if (parser.exito()) {
          ruta = parser.getRuta();
          btnContinuar.setVisibility(View.VISIBLE);
          txtNombre.setText(parser.getRuta().getNombre());
          txtNombre.setEnabled(true);
          cargaFinalizada();
        }
      }
    });
  }

  protected void cargaIniciada() {
    cargandoRuta=true;
    btnImportar.setEnabled(false);
  }

  protected void cargaFinalizada() {
    RutonApp.getInstance().ocultarCargando();
    cargandoRuta=false;
    btnImportar.setEnabled(true);
  }
}
