package com.parmaia.ruton.parsers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

public abstract class XMLParser extends Parser {
  protected String ns="";
  @Override
  protected void doParse(File archivo) {
    try {
      InputStream is = new FileInputStream(archivo);
      try {
        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(is, null);
        parser.nextTag();
        leerXML(parser);
      } catch (XmlPullParserException e) {
        fallo();
        e.printStackTrace();
      } catch (IOException e) {
        fallo();
        e.printStackTrace();
      } finally {
        is.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
      fallo();
    }
  }

  protected void saltar(XmlPullParser xmlParser) throws XmlPullParserException, IOException {
    if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
        throw new IllegalStateException();
    }
    int depth = 1;
    while (depth != 0) {
        switch (xmlParser.next()) {
        case XmlPullParser.END_TAG:
            depth--;
            break;
        case XmlPullParser.START_TAG:
            depth++;
            break;
        }
    }
    while (xmlParser.getEventType()!=XmlPullParser.START_TAG)
      xmlParser.next();
 }
  
  protected String leerTexto(XmlPullParser parser) throws IOException, XmlPullParserException {
    String result = "";
    if (parser.next() == XmlPullParser.TEXT) {
        result = parser.getText();
        parser.nextTag();
    }
    return result;
}
  
  protected abstract void leerXML(XmlPullParser xmlParser);
  
}
