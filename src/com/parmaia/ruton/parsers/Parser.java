package com.parmaia.ruton.parsers;

import java.io.File;

import android.os.AsyncTask;

import com.parmaia.ruton.dao.DaoSession;
import com.parmaia.ruton.dao.Ruta;
import com.parmaia.ruton.util.RutonApp;

public abstract class Parser {
  protected Ruta ruta;
  protected boolean exito;
  
  public interface ParseCallback{
    public void onParseEnd(Parser parser);
    public void onParseFail();
  }
  
  public static String getFormatosSoportados(){
		return "gpx";
	}
	
	public static boolean archivoSoportado(String archivo){
	  return (archivo.toLowerCase().endsWith(getFormatosSoportados()));
	}
	
	public static void parseTrack(final File archivo, final ParseCallback callback){
		if (archivoSoportado(archivo.getName())){
		  new AsyncTask<Void, Void, Parser>(){
        @Override
        protected Parser doInBackground(Void... params) {
          Parser p = new GPXParser();
          p.doParse(archivo);
          return p;
        }
        protected void onPostExecute(Parser p) {
          if (p.exito())
            callback.onParseEnd(p);
          else
            callback.onParseFail();
        };
      }.execute();
		}else{
		  callback.onParseFail();
		}
	}
	
	protected abstract void doParse(File archivo);	
	
	public Parser(){
	  exito = false;
	  ruta = new Ruta();
	}
	
	public Ruta getRuta(){
	  return ruta;
	}
	
	public boolean exito(){
	  return exito;
	}
	protected void fallo(){
	  exito=false;
	}
}
