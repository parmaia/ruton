package com.parmaia.ruton.parsers;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.parmaia.ruton.dao.CoordRuta;

public class GPXParser extends XMLParser {

  @Override
  protected void leerXML(XmlPullParser xmlParser) {
    try {
      xmlParser.require(XmlPullParser.START_TAG, ns, "gpx");
      xmlParser.nextTag();
      while (!xmlParser.getName().equals("trk")){
        saltar(xmlParser);
      }
      xmlParser.require(XmlPullParser.START_TAG, ns, "trk");
      while ( !(xmlParser.next() == XmlPullParser.END_TAG && ("trk".equals(xmlParser.getName()))) ) {
        if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
          continue;
        }
        String name = xmlParser.getName();

        if (name.equals("name")) {
          ruta.setNombre(leerTexto(xmlParser));
        } else if (name.equals("trkseg")) {
          leerCoordenadas(xmlParser);
        }
      }
      exito=true;
    } catch (XmlPullParserException e) {
      fallo();
      e.printStackTrace();
    } catch (IOException e) {
      fallo();
      e.printStackTrace();
    }
  }
  
  private void leerCoordenadas(XmlPullParser xmlParser) throws XmlPullParserException, IOException{
    xmlParser.require(XmlPullParser.START_TAG, ns, "trkseg");
    CoordRuta coord=new CoordRuta(0d,0d,0d,null);
    while ( !(xmlParser.next() == XmlPullParser.END_TAG && ("trkseg".equals(xmlParser.getName()))) ) {
      if (xmlParser.getEventType() == XmlPullParser.END_TAG && "trkpt".equals(xmlParser.getName())){
        ruta.addCoord(coord);
        coord = new CoordRuta(0d,0d,0d,null);
      }
      if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
        continue;
      }
      String name = xmlParser.getName();

      if (name.equals("trkpt")) {
        coord.setLat(Double.parseDouble(xmlParser.getAttributeValue(null, "lat")));
        coord.setLon(Double.parseDouble(xmlParser.getAttributeValue(null, "lon")));
      } else if (name.equals("ele")) {
        coord.setAlt(Double.parseDouble(leerTexto(xmlParser)));
      } else if (name.equals("time")) {
        coord.setTiempo(leerTexto(xmlParser));
      }
    }
  }

}
