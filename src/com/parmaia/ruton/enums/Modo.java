package com.parmaia.ruton.enums;

public enum Modo {
  NINGUNO, CREAR, SEGUIR;
  public static Modo fromString(String modo) {
    if (modo.equals("crear"))
      return CREAR;
    else if (modo.equals("seguir"))
      return SEGUIR;
    else
      return NINGUNO;
  }
  
  public String toString(){
    switch(this){
      case CREAR:
        return "crear";
      case NINGUNO:
        return "ninguno";
      case SEGUIR:
        return "seguir";
    }
    return "";
  }
  
  public static String tag(){
    return "modo";
  }
}
