package com.parmaia.ruton.entidades;

import com.google.android.gms.maps.model.Marker;

public class Indicacion {
  private Marker marca;
  private String texto;
  
  public Indicacion(){
    this(null,"");
  }
  
  public Indicacion(Marker marca){
    this(marca,"");
  }
  
  public Indicacion(Marker marca, String texto){
    setMarca(marca);
    setTexto(texto);
  }
  
  public Marker getMarca() {
    return marca;
  }
  public void setMarca(Marker marca) {
    this.marca = marca;
  }
  public String getTexto() {
    return texto;
  }
  public void setTexto(String texto) {
    this.texto = texto;
  }
  
  
  
}
