package com.parmaia.ruton.servicios;

import java.util.List;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;
import com.parmaia.ruton.util.Mensaje;

import android.app.IntentService;
import android.content.Intent;

public class ServicioRuton extends IntentService{
  public static final String KEY_EVENTO       = "Accion_Servicio";
  public static final int EVENTO_INDICACION   = 1;
  public static final int EVENTO_LOCALIZACION = 2;
  
  public ServicioRuton() {
    super("ServicioRuton");
  }
  
  @Override
  protected void onHandleIntent(Intent intent) {
    switch(intent.getExtras().getInt(KEY_EVENTO)){
      case EVENTO_INDICACION:
        eventoIndicacion(intent);
        break;
      case EVENTO_LOCALIZACION:
        break;
    }
  }

  private void eventoIndicacion(Intent intent){
    // First check for errors
    if (LocationClient.hasError(intent)) {
      // Get the error code with a static method
      int errorCode = LocationClient.getErrorCode(intent);
      // Log the error
      Mensaje.largo(getApplicationContext(), "Location Services error: " + Integer.toString(errorCode));
      /*
       * You can also send the error code to an Activity or
       * Fragment with a broadcast Intent
       */
    } else {
      // Get the type of transition (entry or exit)
      int transitionType = LocationClient.getGeofenceTransition(intent);
      // Test that a valid transition was reported
      if (
          (transitionType == Geofence.GEOFENCE_TRANSITION_ENTER)
           ||
          (transitionType == Geofence.GEOFENCE_TRANSITION_EXIT)
         ) {
         List <Geofence> triggerList = LocationClient.getTriggeringGeofences(intent);
         Mensaje.largo(getApplicationContext(), "Geofence Detectado");
//
//          String[] triggerIds = new String[triggerList.size()];
//
//          for (int i = 0; i < triggerIds.length; i++) {
//              // Store the Id of each geofence
//              triggerIds[i] = triggerList.get(i).getRequestId();
//          }
          /*
           * At this point, you can store the IDs for further use
           * display them, or display the details associated with
           * them.
           */
      }else {// An invalid transition was reported
        Mensaje.largo(getApplicationContext(), "Geofence transition error: " + Integer.toString(transitionType));
      }
    }
  }
}
