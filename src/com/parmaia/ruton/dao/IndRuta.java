package com.parmaia.ruton.dao;

import com.google.android.gms.location.Geofence;
import com.parmaia.ruton.dao.DaoSession;
import com.parmaia.ruton.util.RutonApp;

import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table IND_RUTA.
 */
public class IndRuta implements java.io.Serializable {

    private Long id;
    private Long rutaId;
    private Double lat;
    private Double lon;
    private String texto;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient IndRutaDao myDao;

    private Ruta ruta;
    private Long ruta__resolvedKey;


    // KEEP FIELDS - put your custom fields here
    private static final long serialVersionUID = 7689198906810049320L;
    // KEEP FIELDS END

    public IndRuta() {
    }

    public IndRuta(Long id) {
        this.id = id;
    }

    public IndRuta(Long id, Long rutaId, Double lat, Double lon, String texto) {
        this.id = id;
        this.rutaId = rutaId;
        this.lat = lat;
        this.lon = lon;
        this.texto = texto;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getIndRutaDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRutaId() {
        return rutaId;
    }

    public void setRutaId(Long rutaId) {
        this.rutaId = rutaId;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    /** To-one relationship, resolved on first access. */
    public Ruta getRuta() {
        Long __key = this.rutaId;
        if (ruta__resolvedKey == null || !ruta__resolvedKey.equals(__key)) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            RutaDao targetDao = daoSession.getRutaDao();
            Ruta rutaNew = targetDao.load(__key);
            synchronized (this) {
                ruta = rutaNew;
            	ruta__resolvedKey = __key;
            }
        }
        return ruta;
    }

    public void setRuta(Ruta ruta) {
        synchronized (this) {
            this.ruta = ruta;
            rutaId = ruta == null ? null : ruta.getId();
            ruta__resolvedKey = rutaId;
        }
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

    // KEEP METHODS - put your custom methods here

    public Geofence getGeofence() {
      return new Geofence.Builder()
      .setRequestId(String.valueOf(getId()))
      .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
      .setCircularRegion(
              getLat(), getLon(), RutonApp.getInstance().getConfig().radioIndicacion)
      .setExpirationDuration(Geofence.NEVER_EXPIRE)
      .build();
    }

    // KEEP METHODS END

}
