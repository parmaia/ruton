package com.parmaia.ruton.dao;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table INDICACION.
 */
public class Indicacion {

    private Long id;
    private String texto;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public Indicacion() {
    }

    public Indicacion(Long id) {
        this.id = id;
    }

    public Indicacion(Long id, String texto) {
        this.id = id;
        this.texto = texto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
