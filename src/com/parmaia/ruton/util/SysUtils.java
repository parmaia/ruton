package com.parmaia.ruton.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Looper;

import com.parmaia.ruton.R;
import com.parmaia.ruton.dialogos.DialogoSiNo;
import com.parmaia.ruton.dialogos.DialogoSiNo.DialogoSiNoListener;

public class SysUtils {

  public static boolean isGPSOn(final Activity parent, boolean mostrarDialogo) {
    final LocationManager manager = (LocationManager) parent
        .getSystemService(Context.LOCATION_SERVICE);
    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
      if (mostrarDialogo) {
        (new DialogoSiNo(parent))
            .setTitulo(
                parent.getResources().getString(R.string.gps_no_activado))
            .setDialogoSiNoListener(new DialogoSiNoListener() {
              @Override
              public void onCancelar() {
              }

              @Override
              public void OnAceptar() {
                parent.startActivity(new Intent(
                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
              }
            }).show();
      }
      return false;
    } else
      return true;
  }
  
  public static boolean isOnUIThread(){
    if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
      // On UI thread.
      return true;
    } else {
      //Not UI thread
      return false;
    }
  }
}
