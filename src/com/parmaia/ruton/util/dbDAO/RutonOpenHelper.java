package com.parmaia.ruton.util.dbDAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import com.parmaia.ruton.R;
import com.parmaia.ruton.dao.DaoMaster;
import com.parmaia.ruton.dao.IndicacionDao;
import com.parmaia.ruton.dao.DaoMaster.OpenHelper;
import com.parmaia.ruton.dao.Indicacion;
import com.parmaia.ruton.util.Log;
import com.parmaia.ruton.util.RutonApp;

public class RutonOpenHelper extends OpenHelper {

	public RutonOpenHelper(Context context, String name, CursorFactory factory) {
		super(context, name, factory);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
    super.onCreate(db);
	  String [] indicaciones = RutonApp.getInstance().getResources().getStringArray(R.array.indicaciones_precargadas);
	  
	  StringBuilder sb = new StringBuilder();
	  sb.append("Insert into ").
	     append(IndicacionDao.TABLENAME).
	     append(" (TEXTO) values ");
	  for (int i=0;i<indicaciones.length;i++){
	    sb.append("('").append(indicaciones[i]).append("')");
	    if (i<indicaciones.length-1)
	      sb.append(", ");
	  }
	  db.execSQL(sb.toString());
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		//Para la version 2 de la base de datos, esto DEBE cambiarse.
		Log.d("onUpgrade","old: ",oldVersion," new: ",newVersion);
		if (oldVersion == 1 && newVersion >1){
			throw new AssertionError("HAY QUE CAMBIAR LA LOGICA DE ACTUALIZACION DE LA BD");
		}else{
			Log.i("greenDAO", "Upgrading schema from version " + oldVersion + " to " + newVersion + " by dropping all tables");
	        DaoMaster.dropAllTables(db, true);
	        DaoMaster.createAllTables(db, false);
		}
	}

}
