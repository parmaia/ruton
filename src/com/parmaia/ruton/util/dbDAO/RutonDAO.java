package com.parmaia.ruton.util.dbDAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.parmaia.ruton.dao.DaoMaster;
import com.parmaia.ruton.dao.DaoSession;
import com.parmaia.ruton.dao.Ruta;

public class RutonDAO {
	private DaoMaster daoMaster;
	private DaoSession daoSession;
	
	public void init(Context context){
		RutonOpenHelper helper = new RutonOpenHelper(context, "ruton", null);
		SQLiteDatabase db = helper.getWritableDatabase();
		setDaoMaster(new DaoMaster(db));
		setDaoSession(getDaoMaster().newSession());
	}

	public DaoMaster getDaoMaster() {
		return daoMaster;
	}

	private void setDaoMaster(DaoMaster daoMaster) {
		this.daoMaster = daoMaster;
	}

	public DaoSession getDaoSession() {
		return daoSession;
	}

	private void setDaoSession(DaoSession daoSession) {
		this.daoSession = daoSession;
	}
 
}


//////EJEMPLOS DE USO:

//Obtengo las referencias a los DAO.
//DescriptorDao ddao = getRutonDao().getDaoSession().getDescriptorDao();
//PaqueteDao    pdao = getRutonDao().getDaoSession().getPaqueteDao();

//Busca un paquete y si no lo encuentra inserta uno nuevo.		
//Paquete p = pdao.queryBuilder().where(PaqueteDao.Properties.Sku.eq("aaab")).unique();
//if (p==null){
//	p = new Paquete();
//	p.setSku("aaab");
//	p.setMedia("general");
//	pdao.insert(p);
//}

//Inserta un descriptor nuevo (si se va a guardar con paquete, el paquete DEBE haberse guardado antes).		
//Descriptor d = new Descriptor();
//d.setSku(p.getSku());
//d.setFrase("¡Arrecho!");
//d.setIcono("icono_molon_3");
//d.setPaquete(p);
//d.setRecurso("molon-4");
//d.setResourceId("molon-4");
//d.setTipo("a");
//d.setUrl("http://parmaia.com/recurso/molon");
//d.setVoltear(true);
//ddao.insert(d);

//Busca todos los descriptores de un paquete.		
//List<Descriptor> ds = p.getDescriptorList();

//Buscar un resourceID.		
//Descriptor d = ddao.queryBuilder().where(DescriptorDao.Properties.ResourceId.eq("molon-2")).unique();


