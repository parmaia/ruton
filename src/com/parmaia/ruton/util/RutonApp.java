package com.parmaia.ruton.util;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.parmaia.ruton.R;
import com.parmaia.ruton.dao.Ruta;
import com.parmaia.ruton.util.dbDAO.RutonDAO;

/**
 * Created by parmaia on 10/07/13.
 */
public class RutonApp extends Application {
    public static final boolean DEBUG = true;
    private static RutonApp instance;
    private boolean mIniciado=false;
    Dialog dialogo;
    RutonDAO rutonDao;
    private Ruta rutaActual;
    private int indicacionActual;
    private Config config;
    private boolean mHaPasadoPorUnPunto;
    private Handler handlerUI = new Handler();

    public static RutonApp getInstance() {
        return instance;
    }

    public RutonApp() {
        super();
        instance = this;
        config = new Config();
    }
    
    @Override
    public void onCreate() {
      getRutonDao();
      super.onCreate();
    }

    public boolean isIniciado() {
        return mIniciado;
    }

    public void setIniciado(boolean iniciado) {
        this.mIniciado = iniciado;
    }
    
    public RutonDAO getRutonDao() {
      if (rutonDao == null){
        rutonDao = new RutonDAO();
        rutonDao.init(this);
      }
      return rutonDao;
    }
    
    public void mostrarCargando(Activity act) {
      mostrarCargando(act, "");
    }
    
    public void mostrarCargando(Activity act, String mensaje) {
      if (dialogo!=null)
        ocultarCargando();
      dialogo = new Dialog(act, R.style.dialogo_transparente );
      dialogo.setCancelable(false);
      dialogo.show();
      dialogo.setContentView(R.layout.cargando);
      if (!mensaje.equals(""))
        ((TextView) dialogo.findViewById(R.id.mensaje)).setText(mensaje);
      else
        dialogo.findViewById(R.id.mensaje).setVisibility(View.GONE);
    }
    
    public void ocultarCargando(){
      if (dialogo!=null ) {
        if (dialogo.isShowing()) {
          try{
            dialogo.dismiss();
            dialogo=null;
          } catch (Exception e){}
        }
      }
    }

    public Ruta getRutaActual() {
      return rutaActual;
    }

    public void setRutaActual(Ruta rutaActual) {
      this.rutaActual = rutaActual;
    }

    public int getIndicacionActual() {
      return indicacionActual;
    }

    public void setIndicacionActual(int indicacionActual) {
      this.indicacionActual = indicacionActual;
    }
    
    public void iniciarRuta(){
      setIndicacionActual(0);
      this.mHaPasadoPorUnPunto = false;
    }
    
    public boolean haPasadoPorUnPunto(){
      return mHaPasadoPorUnPunto;
    }
    
    public void setHaPasadoPorUnPunto(){
      this.mHaPasadoPorUnPunto = true;
    }

    public Config getConfig() {
      return config;
    }
    
    public void post(Runnable r){
      handlerUI.post(r);
    }
    
    public void postDelayed(Runnable r, int delayMillis){
      handlerUI.postDelayed(r, delayMillis);
    }
}
