package com.parmaia.ruton.util;

import android.content.Context;
import android.widget.Toast;

public class Mensaje {
	public static final int DURACION_CORTO = Toast.LENGTH_SHORT;
	public static final int DURACION_LARGO = Toast.LENGTH_LONG;
	
	public static void corto(Context context, final String mensaje){
	  doShow(context , mensaje, DURACION_CORTO);
	}

	public static void corto(Context context, int mensajeId){
	  doShow(context , mensajeId, DURACION_CORTO);
	}

	public static void largo(Context context, String mensaje){
	  doShow(context , mensaje, DURACION_LARGO);
	}

	public static void largo(Context context, int mensajeId){
	  doShow(context , mensajeId, DURACION_LARGO);
	}	
	
  private static void doShow(final Context context, final String mensaje, final int duracion){
    if (SysUtils.isOnUIThread()) {
      // On UI thread.
      Toast.makeText(context , mensaje, duracion).show();
    } else {
      //Not UI thread
      RutonApp.getInstance().post(new Runnable() {
        @Override
        public void run() {
          Toast.makeText(context , mensaje, duracion).show();
        }
      });
    }
  }
  private static void doShow(final Context context, final int idMensaje, final int duracion){
    if (SysUtils.isOnUIThread()) {
      // On UI thread.
      Toast.makeText(context , idMensaje, duracion).show();
    } else {
      //Not UI thread
      RutonApp.getInstance().post(new Runnable() {
        @Override
        public void run() {
          Toast.makeText(context , idMensaje, duracion).show();
        }
      });
    }
  }
}
