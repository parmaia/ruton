package com.parmaia.ruton.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

  public static Date parseDate(String dateStr) throws ParseException{
    Date date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")).parse(dateStr.replaceAll("Z$", "+0000"));
    return date;
  }
  
//  String defaultTimezone = TimeZone.getDefault().getID();
//
//  System.out.println("string: " + dateStr);
//  System.out.println("defaultTimezone: " + defaultTimezone);
//  System.out.println("date: " + (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).format(date));
  
}
