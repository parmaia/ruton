package com.parmaia.ruton.util;

public class Log {
  
  public static void e(Exception e){
    if (RutonApp.DEBUG)
      e.printStackTrace();
  }

  public static void d(Object donde, Object ...objects){
    Log.d(donde.getClass().getSimpleName(),objects);
  }
  
  public static void d(String Tag,Object ...objects){
    if (RutonApp.DEBUG){
      StringBuffer sf = unit(objects);
      android.util.Log.d(Tag,sf.toString());
    }
  }
  
  public static void i(String Tag,Object ...objects){
    if (RutonApp.DEBUG){
      StringBuffer sf = unit(objects);
      android.util.Log.i(Tag,sf.toString());
    }
  }

  private static StringBuffer unit(Object... objects) {
    StringBuffer sf = new StringBuffer();
    for (int i=0;i<objects.length;i++){
      sf.append(objects[i]==null?"null":String.valueOf(objects[i]));
    }
    return sf;
  }
  
  public static void l(){
    n(1," ");
  }
  
  public static void l(Object ...objects){
    n(1,objects);
  }

  /**
   * Log con identificacion automatica del punto de ejecucion.
   * 
   * @param nivel Nivel en el stacktrace en el que se esta ejecutando Log.l() (por defecto es 0->1er nivel).
   * @param objects Objetos a mostrar.
   */
  public static void n(int nivel, Object ...objects){
    int pos = nivel+3;
    String fullClassName = Thread.currentThread().getStackTrace()[pos].getClassName();            
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = Thread.currentThread().getStackTrace()[pos].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[pos].getLineNumber();
        d(className+"("+methodName+": "+lineNumber+")",unit(objects).toString());
  }
  
  public static void v(String tag, String msg) {
    android.util.Log.v(tag,msg);
  }

  public static void e(String tag, String msg) {
    android.util.Log.e(tag,msg);
  }

  public static void e(String tag, String msg, Throwable tr) {
    android.util.Log.e(tag, msg, tr);   
  }
  
  /**
   * Muestra desde donde se esta haciendo el llamado (el stacktrace) hasta la profundidad indicada.
   * @param profundidad
   */
  public static void s(String tag, int profundidad){
    int min =3;
    StackTraceElement arr[] = Thread.currentThread().getStackTrace();
    String trace[] = new String[profundidad];
    
    for (int i=min;i<profundidad+min && i< arr.length ;i++){
      trace[i-min] = arr[i].getClassName()+"."+arr[i].getMethodName()+"\n";
    }
    d(tag, trace);
  }
}

