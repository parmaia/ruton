package com.parmaia.ruton.adapters;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.parmaia.ruton.R;

public class MiInfoWindowAdapter implements InfoWindowAdapter {
  private Activity activity;

  public MiInfoWindowAdapter(Activity activity) {
    super();
    this.activity = activity;
  }

  @Override
  public View getInfoContents(Marker marker) {
    View v = activity.getLayoutInflater().inflate(R.layout.marker, null);
    ((TextView) v.findViewById(R.id.titulo)).setText(marker.getTitle());
    ((TextView) v.findViewById(R.id.snippet)).setText(marker.getSnippet());
    int resId;
    if (marker.getTitle().equals(
        activity.getResources().getString(R.string.inicio)))
      resId = R.drawable.pin_verde;
    else if (marker.getTitle().equals(
        activity.getResources().getString(R.string.fin)))
      resId = R.drawable.pin_rojo;
    else
      resId = R.drawable.pin_azul;
    ((ImageView) v.findViewById(R.id.icono)).setImageResource(resId);

    return v;
  }

  @Override
  public View getInfoWindow(Marker marker) {
    return null;
  }
}