package com.parmaia.ruton.dialogos;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.parmaia.ruton.R;

public class DialogoSeleccionArchivo {
  private static final String PARENT_DIR = "..";
  private final String TAG = getClass().getName();
  private String[] fileList;
  private File currentPath;

  public interface FileSelectedListener {
    void fileSelected(File file);
    void cancelDialog();
  }

  public interface DirectorySelectedListener {
    void directorySelected(File directory);
  }

  private ListenerList<FileSelectedListener> fileListenerList = new ListenerList<DialogoSeleccionArchivo.FileSelectedListener>();
  private ListenerList<DirectorySelectedListener> dirListenerList = new ListenerList<DialogoSeleccionArchivo.DirectorySelectedListener>();
  private final Activity activity;
  private boolean selectDirectoryOption;
  private String fileEndsWith;
  private Dialog dialog;
  private TextView titulo;
  private ArrayAdapter<String> adapter;
  private ListView lista;

  public DialogoSeleccionArchivo(Activity activity, String fileEnds) {
    this(activity, null, fileEnds);
  }

  /**
   * @param activity
   * @param initialPath
   * @param fileEnd
   *          filter
   */
  public DialogoSeleccionArchivo(Activity activity, File path, String fileEnds) {
    this.activity = activity;
    fileEndsWith = fileEnds;
    if (path == null || !path.exists())
      path = Environment.getExternalStorageDirectory();
    loadFileList(path);
  }

  /**
   * @return file dialog
   */
  public Dialog createFileDialog() {
    dialog = new Dialog(activity, R.style.dialogo_transparente);

    LayoutInflater inflater = activity.getLayoutInflater();
    View layout = inflater.inflate(R.layout.dialogo_seleccionar_archivo, null);
    titulo = (TextView) layout.findViewById(R.id.titulo);
    titulo.setText(currentPath.getPath());

    dialog.setContentView(layout);

    // builder.setTitle(currentPath.getPath());
    // if (selectDirectoryOption) {
    // builder.setPositiveButton("Select directory", new OnClickListener() {
    // public void onClick(DialogInterface dialog, int which) {
    // Log.d(TAG, currentPath.getPath());
    // fireDirectorySelectedEvent(currentPath);
    // }
    // });
    // }
    
    lista = (ListView) layout.findViewById(R.id.lista);
    adapter = new ArrayAdapter<String>(activity, R.layout.item_lista_alto, R.id.texto, fileList);
    lista.setAdapter(adapter);
    lista.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> arg0, View view, int pos, long id) {
        String fileChosen = fileList[pos];
        File chosenFile = getChosenFile(fileChosen);
        if (chosenFile.isDirectory()) {
          loadFileList(chosenFile);
          adapter = new ArrayAdapter<String>(activity, R.layout.item_lista_alto, R.id.texto, fileList);
          lista.setAdapter(adapter);
          titulo.setText(currentPath.getPath());          
        } else{
          dialog.dismiss();
          fireFileSelectedEvent(chosenFile);
        }
      }
    });
    dialog.setOnCancelListener(new OnCancelListener() {      
      @Override
      public void onCancel(DialogInterface dialog) {
        fireCancelDialogEvent();
      }
    });

    // builder.setItems(fileList, new DialogInterface.OnClickListener() {
    // public void onClick(DialogInterface dialog, int which) {
    // String fileChosen = fileList[which];
    // File chosenFile = getChosenFile(fileChosen);
    // if (chosenFile.isDirectory()) {
    // loadFileList(chosenFile);
    // dialog.cancel();
    // dialog.dismiss();
    // showDialog();
    // } else
    // fireFileSelectedEvent(chosenFile);
    // }
    // });

    return dialog;
  }

  public void addFileListener(FileSelectedListener listener) {
    fileListenerList.add(listener);
  }

  public void removeFileListener(FileSelectedListener listener) {
    fileListenerList.remove(listener);
  }

  public void setSelectDirectoryOption(boolean selectDirectoryOption) {
    this.selectDirectoryOption = selectDirectoryOption;
  }

  public void addDirectoryListener(DirectorySelectedListener listener) {
    dirListenerList.add(listener);
  }

  public void removeDirectoryListener(DirectorySelectedListener listener) {
    dirListenerList.remove(listener);
  }

  /**
   * Show file dialog
   */
  public void showDialog() {
    createFileDialog().show();
  }

  private void fireFileSelectedEvent(final File file) {
    fileListenerList
        .fireEvent(new FireHandler<DialogoSeleccionArchivo.FileSelectedListener>() {
          public void fireEvent(FileSelectedListener listener) {
            listener.fileSelected(file);
          }
        });
  }
  
  private void fireCancelDialogEvent() {
    fileListenerList
        .fireEvent(new FireHandler<DialogoSeleccionArchivo.FileSelectedListener>() {
          public void fireEvent(FileSelectedListener listener) {
            listener.cancelDialog();;
          }
        });
  }

  private void fireDirectorySelectedEvent(final File directory) {
    dirListenerList
        .fireEvent(new FireHandler<DialogoSeleccionArchivo.DirectorySelectedListener>() {
          public void fireEvent(DirectorySelectedListener listener) {
            listener.directorySelected(directory);
          }
        });
  }

  private void loadFileList(File path) {
    this.currentPath = path;
    List<String> r = new ArrayList<String>();
    if (path.exists()) {
      if (path.getParentFile() != null)
        r.add(PARENT_DIR);
      FilenameFilter filter = new FilenameFilter() {
        public boolean accept(File dir, String filename) {
          File sel = new File(dir, filename);
          if (!sel.canRead())
            return false;
          if (selectDirectoryOption)
            return sel.isDirectory();
          else {
            boolean endsWith = fileEndsWith != null ? filename.toLowerCase()
                .endsWith(fileEndsWith) : true;
            boolean hidden = filename.startsWith(".") && filename.length() > 2;
            return (endsWith || sel.isDirectory()) && !hidden;
          }
        }
      };
      String[] fileList1 = path.list(filter);
      for (String file : fileList1) {
        r.add(file);
      }
      Collections.sort(r, new Comparator<String>() {
        @Override
        public int compare(String arg0, String arg1) {
          return arg0.compareToIgnoreCase(arg1);
        }
      });
    }
    fileList = (String[]) r.toArray(new String[] {});
  }

  private File getChosenFile(String fileChosen) {
    if (fileChosen.equals(PARENT_DIR))
      return currentPath.getParentFile();
    else
      return new File(currentPath, fileChosen);
  }

  public void setFileEndsWith(String fileEndsWith) {
    this.fileEndsWith = fileEndsWith != null ? fileEndsWith.toLowerCase(Locale.US)
        : fileEndsWith;
  }
  
  private interface FireHandler<L> {
    void fireEvent(L listener);
  }
  private class ListenerList<L> {
    private List<L> listenerList = new ArrayList<L>();

    public void add(L listener) {
      listenerList.add(listener);
    }

    public void fireEvent(FireHandler<L> fireHandler) {
      List<L> copy = new ArrayList<L>(listenerList);
      for (L l : copy) {
        fireHandler.fireEvent(l);
      }
    }

    public void remove(L listener) {
      listenerList.remove(listener);
    }

    public List<L> getListenerList() {
      return listenerList;
    }
  }
}

