package com.parmaia.ruton.dialogos;

import java.util.List;

import com.parmaia.ruton.R;
import com.parmaia.ruton.dao.Indicacion;
import com.parmaia.ruton.util.Mensaje;
import com.parmaia.ruton.util.RutonApp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class DialogoIndicaciones extends Dialog implements android.view.View.OnClickListener{
  private DialogoIndicacionesListener listener;
  private EditText textoInput=null;
  private TextView textoTitulo=null;
  private String textoTemp="";
  private String tituloTemp="";
  private int visibilidadSubirBajar = View.VISIBLE;

  public DialogoIndicaciones(Context context) {
    super(context, R.style.dialogo_transparente);
  }
  
  public DialogoIndicaciones setDialogoIndicacionesListener(DialogoIndicacionesListener listener){
    this.listener = listener;
    return this;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.dialogo_indicaciones);
    Button aceptar = (Button) findViewById(R.id.aceptar);
    Button cancelar = (Button) findViewById(R.id.cancelar);
    aceptar.setOnClickListener(this);
    cancelar.setOnClickListener(this);
    textoTitulo = (TextView) findViewById(R.id.titulo);
    textoTitulo.setText(tituloTemp);
    textoInput = (EditText) findViewById(R.id.texto_input);
    textoInput.setText(textoTemp);
    textoInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View v, boolean hasFocus) {
          if (hasFocus) {
              DialogoIndicaciones.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
          }
      }
    });
    textoInput.requestFocus();
    setOnCancelListener(new OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        if (listener!=null) listener.onCancelar();
      }
    });
    View subir = findViewById(R.id.subir);
    subir.setVisibility(visibilidadSubirBajar);
    subir.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (listener!=null) listener.onSubir();
      }
    });
    View bajar = findViewById(R.id.bajar);
    bajar.setVisibility(visibilidadSubirBajar);
    bajar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (listener!=null) listener.onBajar();
      }
    });
    List<Indicacion> inds=RutonApp.getInstance().getRutonDao().getDaoSession().getIndicacionDao().loadAll();
    final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.item_lista,R.id.texto);
    for(Indicacion i : inds){
      adapter.add(i.getTexto());
    }
    ListView lista =(ListView) findViewById(R.id.lista);
    lista.setAdapter(adapter);
    lista.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> viewGroup, View v, int pos, long id) {
        textoInput.setText(adapter.getItem(pos));
      }
    });
  }
  
  @Override
  public void onClick(View v) {
    switch (v.getId()) {
    case R.id.aceptar:
      if (textoInput.getText().equals(""))
        Mensaje.largo(getContext(), getContext().getResources().getString(R.string.campo_vacio));
      else
        if (listener!=null) listener.onAceptar(textoInput.getText().toString());
      break;
    case R.id.cancelar:
      if (listener!=null) listener.onCancelar();
      break;
    }
    dismiss();
  }
  
  public interface DialogoIndicacionesListener{
    public void onAceptar(String texto);
    public void onCancelar();
    public void onBajar();
    public void onSubir();
  }

  public DialogoIndicaciones setTexto(String texto) {
    if (textoInput==null)
      textoTemp = texto;
    else
      textoInput.setText(texto);
    return this;
  }

  public DialogoIndicaciones setTitulo(String titulo) {
    if (textoTitulo==null)
      this.tituloTemp = titulo;
    else
      textoTitulo.setText(titulo);
    return this;
  }

  public DialogoIndicaciones ocultarSubirBajar() {
    visibilidadSubirBajar=View.GONE;
    return this;
  }
  
}
