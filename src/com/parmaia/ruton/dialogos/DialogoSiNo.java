package com.parmaia.ruton.dialogos;

import com.parmaia.ruton.R;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class DialogoSiNo extends Dialog implements android.view.View.OnClickListener{
  private DialogoSiNoListener listener;
  private TextView textoTitulo=null;
  private String tituloTemp="";

  public DialogoSiNo(Context context) {
    super(context, R.style.dialogo_transparente);
  }
  
  public DialogoSiNo setDialogoSiNoListener(DialogoSiNoListener listener){
    this.listener = listener;
    return this;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.dialogo_si_no);
    Button aceptar = (Button) findViewById(R.id.aceptar);
    Button cancelar = (Button) findViewById(R.id.cancelar);
    aceptar.setOnClickListener(this);
    cancelar.setOnClickListener(this);
    textoTitulo = (TextView) findViewById(R.id.titulo);
    textoTitulo.setText(tituloTemp);
    setOnDismissListener(new OnDismissListener() {      
      @Override
      public void onDismiss(DialogInterface dialog) {
        if (listener!=null) listener.onCancelar();
      }
    });
  }
  
  @Override
  public void onClick(View v) {
    switch (v.getId()) {
    case R.id.aceptar:
      if (listener!=null) listener.OnAceptar();
      break;
    case R.id.cancelar:
      if (listener!=null) listener.onCancelar();
      break;
    }
    dismiss();
  }
  
  public interface DialogoSiNoListener{
    public void OnAceptar();
    public void onCancelar();
  }

  public DialogoSiNo setTitulo(String titulo) {
    if (textoTitulo==null)
      this.tituloTemp = titulo;
    else
      textoTitulo.setText(titulo);
    return this;
  }
  
}
