package com.parmaia.ruton.dialogos;

import com.parmaia.ruton.R;
import com.parmaia.ruton.util.Mensaje;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class DialogoInput extends Dialog implements android.view.View.OnClickListener{
  private DialogoInputListener listener;
  private EditText textoInput=null;
  private TextView textoTitulo=null;
  private String textoTemp="";
  private String tituloTemp="";

  public DialogoInput(Context context) {
    super(context, R.style.dialogo_transparente);
  }
  
  public DialogoInput setDialogoInputListener(DialogoInputListener listener){
    this.listener = listener;
    return this;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.dialogo_input);
    Button aceptar = (Button) findViewById(R.id.aceptar);
    Button cancelar = (Button) findViewById(R.id.cancelar);
    aceptar.setOnClickListener(this);
    cancelar.setOnClickListener(this);
    textoTitulo = (TextView) findViewById(R.id.titulo);
    textoTitulo.setText(tituloTemp);
    textoInput = (EditText) findViewById(R.id.texto_input);
    textoInput.setText(textoTemp);
    textoInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View v, boolean hasFocus) {
          if (hasFocus) {
              DialogoInput.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
          }
      }
    });
    textoInput.requestFocus();
    setOnCancelListener(new OnCancelListener() {      
      @Override
      public void onCancel(DialogInterface dialog) {
        if (listener!=null) listener.onCancelar();
      }
    });    
  }
  
  @Override
  public void onClick(View v) {
    switch (v.getId()) {
    case R.id.aceptar:
      if (textoInput.getText().equals(""))
        Mensaje.largo(getContext(), getContext().getResources().getString(R.string.campo_vacio));
      else
        if (listener!=null) listener.onAceptar(textoInput.getText().toString());
      break;
    case R.id.cancelar:
      if (listener!=null) listener.onCancelar();
      break;
    }
    dismiss();
  }
  
  public interface DialogoInputListener{
    public void onAceptar(String texto);
    public void onCancelar();
  }

  public DialogoInput setTexto(String texto) {
    if (textoInput==null)
      textoTemp = texto;
    else
      textoInput.setText(texto);
    return this;
  }

  public DialogoInput setTitulo(String titulo) {
    if (textoTitulo==null)
      this.tituloTemp = titulo;
    else
      textoTitulo.setText(titulo);
    return this;
  }
  
}
