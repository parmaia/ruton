package com.parmaia.ruton.dao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Schema;

/**
 * IMPORTANTE: CUALQUIER CAMBIO EN LAS ENTIDADES DEBE CONLLEVAR UN AUMENTO EN LA
 * VERSION DE LA BD.
 * 
 * @author parmaia
 * 
 */

public class RutonDAO {
  public static void main(String[] args) throws Exception {
    int VERSION = 1; //ver RutonOpenHelper si se cambia la version!
    Schema schemaRuton = new Schema(VERSION, "com.parmaia.ruton.dao");
    Generador.addEntidades(schemaRuton);
    new DaoGenerator().generateAll(schemaRuton, "../src");

    // Las entidades que se vayan generando deben irse agregando aqui para que
    // se generen.

  }

}
