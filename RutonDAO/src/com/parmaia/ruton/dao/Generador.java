package com.parmaia.ruton.dao;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

/**
 * 
 * Run it as a Java application (not Android).
 * 
 * @author Parmaia
 */
public class Generador {

  public static void addEntidades(Schema schema) {
    schema.enableKeepSectionsByDefault();
    Entity ruta = schema.addEntity("Ruta");
    ruta.implementsSerializable();
    ruta.addIdProperty().index();
    ruta.addStringProperty("nombre").notNull();

//    paquete.addDateProperty("creacion");
//    paquete.addDateProperty("hasta");
//    paquete.addStringProperty("sha");
//    paquete.addStringProperty("titulo");
//    paquete.addBooleanProperty("auto");

    Entity coordenadaRuta = schema.addEntity("CoordRuta");
    coordenadaRuta.implementsSerializable();
    coordenadaRuta.addIdProperty();
    Property rutaIdC = coordenadaRuta.addLongProperty("rutaId").index().getProperty();
    coordenadaRuta.addDoubleProperty("lat");
    coordenadaRuta.addDoubleProperty("lon");
    coordenadaRuta.addDoubleProperty("alt");
    coordenadaRuta.addDateProperty("tiempo");
//    descriptor.addStringProperty("tipo");// (a: animacion, s:audio, e:encuesta)
//    descriptor.addStringProperty("icono");
//    descriptor.addStringProperty("recurso");
//    descriptor.addStringProperty("frase");
//    descriptor.addStringProperty("url");
//    descriptor.addBooleanProperty("voltear");
//    descriptor.addBooleanProperty("oculto");
//    descriptor.addStringProperty("ver");

    Entity indicacionRuta = schema.addEntity("IndRuta");
    indicacionRuta.implementsSerializable();
    indicacionRuta.addIdProperty();
    Property rutaIdI = indicacionRuta.addLongProperty("rutaId").index().getProperty();
    indicacionRuta.addDoubleProperty("lat");
    indicacionRuta.addDoubleProperty("lon");
    indicacionRuta.addStringProperty("texto");    
    
    Entity indicacion = schema.addEntity("Indicacion");
    indicacion.addIdProperty();
//    Property descriptorId = votos.addLongProperty("descriptorId").getProperty();
//    votos.addStringProperty("votoId").notNull().index();
//    votos.addStringProperty("icono");
    indicacion.addStringProperty("texto").indexAsc(null, true);

    coordenadaRuta.addToOne(ruta, rutaIdC);
    ruta.addToMany(coordenadaRuta, rutaIdC);

    indicacionRuta.addToOne(ruta, rutaIdI);
    ruta.addToMany(indicacionRuta, rutaIdI);
  }
}